import { createWebHistory, createRouter } from "vue-router";

import Home from "../pages/Home";
import Login from "../pages/Login";
import Register from "../pages/Register";

export const routes = [
    {
        name: "home",
        path: "/",
        component: Home,
    },
    {
        name: "login",
        path: "/login",
        component: Login
    },
    {
        name: "register",
        path: "/register",
        component: Register
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes: routes,
});

export default router;
